# vsolve

Tool for solving substitution ciphers

## Features
 - [x] Multiple languages support
 - [x] Full UTF-8 support
 - [x] [Jakobsen's algorithm](https://www.researchgate.net/publication/266714630_A_fast_method_for_cryptanalysis_of_substitution_ciphers) solving
 - [x] Index of coincidence calculation
 - [x] Reasonable input normalization

## Usage

Calculate [Index of Coincidence](https://en.wikipedia.org/wiki/Index_of_coincidence) and find the corresponding language (one with lowest proximity)
```
$ vsolve ioc -i input.txt
IoC: 0.058922050155199246

Matching languages:
Proximity	Name	IoC	Path
0.001222	Czech	0.0577	./langs/cs2.toml
0.001348	Czech	0.06027	./langs/cs.toml
```

Solve (or should we say crack) the substitution cipher using Jakobsen's algorithm
```
$ vsolve solve -l cs2 -i input.txt
Key: eaointsrvluckdzpmyhjbfgxwq → tolyegfdiahrpsnbvmxzquwckj
cokdybychzapomnel...
```

## Might be nice to implement
 - adding new languages
 - tools for just basic decryption and encryption
 - fancier solving using trigram/word analysis using custom algorithm (I wanted to do this initialy, but it turns out Jakobsen works fine)
 - completely automatic solve with language detection and solving all in one step
 - polyalphabetic ciphers - Jakobsen's works for them fine as well but you need a different D_matrix for each alphabet and switch them

## Download

[Download binary for Linux](https://gitlab.com/sijisu/vsolve/-/jobs/artifacts/master/raw/vsolve?job=rust-build)

## Primer on Jakobsen's algorithm

Let's consider the following ciphertext:
```
enrienrorxppfjnhcropvihpwvaqperxinrphxqlrpenrorxppfjnhcropvihiqnrxphaxgrdnrorxqvivezivdhiqenrjvipcxohjlenrvowhixmhexvixpxeorhaxexpivepxbcalhixigriexvivuenrenvfwnecvaxjrivxexporhaenrtovenronvvqdrjhaaxelvfdxaairgroarhoibfjnbvorhtvfeenrtovenronvvqenhienhexerkxpephiqenhelvftraviwevxexdxaajvbrthjzevenhecorpriealnravvzrqhenxpdoxpedhejnxexpfidxprrgriuvobrbtropvuenrxiirochoelevefoivuuenrerarpjorriuvobvorenhinhauhinvfolvfvfwneiveevnhgrjvbrnrorevwrenrohiqlvfdxaanhgrevarhgrprchoherallvfjvbohqrnrtvdrqnxpnrhqevyfaxhdxaaarhgruxopedrnhgrhtvfeedrielbxiferphevfoqxpcvphalvfdxaafiqropehiqenhexbfpepehoetlhpzxiwlvfjroehxisfrpexvipxiwrirohaerobpdnhehorlvfcorchorqevqv
```

## Thank you

Thanks to [Tomas Hanzak](http://thanzak.sweb.cz/Download.htm) for his [Cipher-Solver](http://thanzak.sweb.cz/Download.htm) that was the main inspiration for this project. Thanks to [Mgr. Pavel Vondruška](http://cryptoworld.info/vondruska/index.php) for the [Crypto-World magazine](http://cryptoworld.info/) and his work.
