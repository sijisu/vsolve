use anyhow::Result;
use clap::Parser;
use vsolve::App;

fn main() -> Result<()> {
    let app = App::parse();
    app.exec()
}
