use std::io::Write;
use std::path::PathBuf;
use std::fs::{File, read_dir};
use std::io::Read;
use std::path::Path;

use clap::{Args, Parser, Subcommand, ValueHint, ArgEnum};
use anyhow::{Context, Result};
use env_logger::Builder;
use log::Level;
use diacritics::remove_diacritics;

use crate::library;
use crate::helpers;

#[derive(Debug, Parser)]
#[clap(name = "vsolve", version)]
#[clap(about = "A cracker for substitution ciphers\n\nhttps://gitlab.com/sijisu/vsolve", long_about = None)]
pub struct App {
    #[clap(flatten)]
    global_opts: GlobalOpts,

    #[clap(subcommand)]
    command: Command,
}

#[derive(Debug, Subcommand)]
enum Command {
    /// Calculate Index of Coincidence of text and find matching language
    Ioc {
        /// Output just the simple IoC, don't try to detect the language
        #[clap(long, short = 'c')]
        ioconly: bool,
    },
    /// Solve the cipher for the specified language
    Solve {
        /// Language of the cipher plaintext, can be figured out using IoC
        #[clap(long, short, default_value = "cs")]
        language: String,

        /// File where the plaintext should be written
        #[clap(long, short, required = false, value_name = "FILE", value_hint = ValueHint::FilePath)]
        output: Option<PathBuf>,

        /// Output just the key and exit
        #[clap(long, short)]
        keyonly: bool,

        /// Output just the plaintext and exit
        #[clap(long, short)]
        quiet: bool,
    },
    /// Analyze text and find bigram frequencies
    Analyze {
        /// File where the bigram frequencies in the TOML format should be written
        #[clap(long, short, required = false, value_name = "FILE", value_hint = ValueHint::FilePath)]
        output: Option<PathBuf>,
    },
    /// Normalize text
    Normalize {
        /// File where the bigram frequencies in the TOML format should be written
        #[clap(long, short, required = false, value_name = "FILE", value_hint = ValueHint::FilePath)]
        output: Option<PathBuf>,
    },
}

#[derive(Debug, Args)]
struct GlobalOpts {
    /// Verbosity level (can be specified multiple times)
    #[clap(long, short, global = true, parse(from_occurrences))]
    verbose: usize,
    
    /// Languages directory where are the configs of individual languages
    #[clap(long, short = 'd', global = true, default_value = "./langs/", value_name = "DIR", value_hint = ValueHint::DirPath)]
    langdir: PathBuf,

    /// File with input text, if not specified stdin is used
    #[clap(long, short, global = true, required = false, value_name = "FILE", value_hint = ValueHint::FilePath)]
    input: Option<PathBuf>,

    /// Normalization of input file modes
    #[clap(long, short, arg_enum, global = true, default_value_t = Normalization::Standard, value_name = "MODE")]
    norm: Normalization,
}


#[derive(Clone, Debug, ArgEnum)]
enum Normalization {
    Off,
    Standard,
    Whitespace,
    Diacritics,
}


impl App {
    pub fn exec(&self) -> Result<()> {
        // Setup logging
        // We want the base logging level to be Level::Warn
        let mut verbosity = self.global_opts.verbose+2;
        if verbosity < 1 || verbosity > Level::max() as usize {
            verbosity = Level::max() as usize;
        }
        let loglevel: Level = unsafe { ::std::mem::transmute(verbosity) }; // ikr better than match statement
        Builder::new().filter_level(loglevel.to_level_filter()).format_timestamp(None).init();

        // Load input
        let mut input: String = self.load_input()?;

        // Check if lang folder exists
        if !Path::new(&self.global_opts.langdir).is_dir() {
            return Err(anyhow::anyhow!("Provided language directory does not exist"))
        }
        
        match &self.command {
            Command::Ioc { ioconly } => {
                return self.cmd_ioc(&mut input, ioconly);
            }
            Command::Solve { language, output, keyonly, quiet } => {
                return self.cmd_solve(&mut input, language, output, keyonly, quiet);
            }
            Command::Analyze { output } => {
                return self.cmd_analyze(&mut input, output);
            }
            Command::Normalize { output } => {
                return self.cmd_normalize(&mut input, output);
            }
        }
    }

    fn load_input(&self) -> Result<String> {
        match &self.global_opts.input {
            None => {
                return Ok(helpers::load_input_from_stdin()?)
            }
            Some(file) => {
                let mut buffer = String::new();
                File::open(file)
                    .with_context(|| "Unable to open the input file")?
                    .read_to_string(&mut buffer)
                    .with_context(|| "Couldn't read the input file")?;
                return Ok(buffer)
            }
        }
    }

    fn normalize(&self, mode: &Normalization, cfg: Option<&library::language::Config>, input: &mut String) -> Result<()> {
        match mode {
            Normalization::Off => {
                return Ok(())
            }
            Normalization::Whitespace => {
                input.retain(|ch| { !ch.is_whitespace() });
                return Ok(())
            }
            Normalization::Diacritics => {
                *input = input.to_lowercase();
                input.retain(|ch| { ch.is_alphabetic() });
                *input = remove_diacritics(input);
                return Ok(())
            }
            Normalization::Standard => {
                *input = input.to_lowercase();
                match cfg {
                    None => {
                        input.retain(|ch| { ch.is_alphabetic() });
                    }
                    Some(c) => {
                        input.retain(|ch| { !ch.is_whitespace() && c.characters.chars.contains(ch) });
                    }
                }
                return Ok(())
            }
        }
    }

    fn cmd_solve(&self, input: &mut String, language: &str, output: &Option<PathBuf>, keyonly: &bool, quiet: &bool) -> Result<()> {
        log::debug!("Solving with parameters ('{}', '{}', '{:?}', {}, {})", input, language, output, keyonly, quiet);

        let lang_config = self.global_opts.langdir.join(language).with_extension("toml");
        log::debug!("Loading language {:?}", lang_config);

        let cfg = library::language::Config::from_disk(lang_config)?;
        cfg.is_valid()?;

        library::helpers::replace_from_config(input, &cfg)?;
        log::debug!("Replaced from config: {}", input);
        self.normalize(&self.global_opts.norm, Some(&cfg), input)?;
        log::info!("Normalized: {}", input);

        let result = library::jakobsen::solve(&input, &cfg)?;
    
        if !quiet {
            println!("Key: {} → {}", cfg.characters.chars, result);
        }

        if *keyonly {
            return Ok(())
        }
    
        let decrypted = library::helpers::character_substitution(&input, &result, &cfg.characters.chars)?;
    
        match &output {
            None => {
                println!("{}", decrypted);
            }
            Some(file) => {
                File::create(file)
                    .with_context(|| "Unable to open the output file")?
                    .write_all(decrypted.as_bytes())
                    .with_context(|| "Couldn't write to the output file")?;
            }
        }

        Ok(())
    }

    fn load_lang_configs(&self) -> Vec<(PathBuf, Result<library::language::Config>)> {
        let mut cfgs: Vec<(PathBuf, Result<library::language::Config>)> = Vec::new();

        let paths = read_dir(&self.global_opts.langdir).unwrap();

        for path in paths {
            let path_pathbuf = path.unwrap().path();
            cfgs.push((path_pathbuf.to_owned(), library::language::Config::from_disk(path_pathbuf)))
        }

        cfgs
    }

    fn cmd_ioc(&self, input: &mut String, ioconly: &bool) -> Result<()> {
        log::debug!("Calculating IoC ('{}', {})", input, ioconly);

        self.normalize(&self.global_opts.norm, None, input)?;
        log::info!("Normalized: {}", input);

        let text_ioc = library::ioc::calculate_ioc(input)?;

        println!("IoC: {}", text_ioc);

        if *ioconly {
            return Ok(())
        }

        let cfgs = self.load_lang_configs();

        // (IoC proximity, IoC, path to config file, language name)
        let mut language_proximity: Vec<(f64, f64, PathBuf, String)> = Vec::new();

        for (path, cfg) in cfgs {
            let cfg_struct = cfg?;
            language_proximity.push(((cfg_struct.ioc-text_ioc).abs(), cfg_struct.ioc, path, cfg_struct.title.to_owned()))
        }

        language_proximity.sort_by(|a, b| a.0.partial_cmp(&b.0).unwrap());

        println!("\nMatching languages:");
        println!("Proximity\tName\tIoC\tPath");

        for lang in language_proximity {
            println!("{:.06}\t{}\t{}\t{}", lang.0, lang.3, lang.1, lang.2.display())
        }

        Ok(())
    }

    fn cmd_analyze(&self, input: &mut String, output: &Option<PathBuf>) -> Result<()> {
        log::debug!("Analyzing ('{}', {:?})", input, output);

        self.normalize(&self.global_opts.norm, None, input)?;
        log::info!("Normalized: {}", input);

        let frequencies = library::helpers::count_substrings_of_length(input, 2)?;

        let freq_normalized = library::helpers::normalize_frequency_map(frequencies);

        let chars_sorted = library::helpers::sort_chars_by_frequency(input)?;

        let mut result = String::new();

        result.push_str("[characters]\n");

        result.push_str(&("chars = \"".to_string()+&chars_sorted+"\"\n\n"));

        result.push_str("[bigrams]\n");

        for i in freq_normalized {
            result.push_str(&(i.0 + " = " + &i.1.to_string() + "\n"))
        }

        match &output {
            None => {
                print!("{}", result);
            }
            Some(file) => {
                File::create(file)
                    .with_context(|| "Unable to open the output file")?
                    .write_all(result.as_bytes())
                    .with_context(|| "Couldn't write to the output file")?;
            }
        }

        Ok(())
    }

    fn cmd_normalize(&self, input: &mut String, output: &Option<PathBuf>) -> Result<()> {
        log::debug!("Normalizing ('{}', {:?})", input, output);

        self.normalize(&self.global_opts.norm, None, input)?;
        log::info!("Normalized: {}", input);

        match &output {
            None => {
                print!("{}", input);
            }
            Some(file) => {
                File::create(file)
                    .with_context(|| "Unable to open the output file")?
                    .write_all(input.as_bytes())
                    .with_context(|| "Couldn't write to the output file")?;
            }
        }

        Ok(())
    }
}
