use std::collections::BTreeMap;
use anyhow::Result;
use ndarray;
use crate::library::language::Config;

type FreqMap = BTreeMap<String, f64>;
type DistributionMatrix = ndarray::Array2<f64>;

pub fn character_substitution(text: &str, old: &str, new: &str) -> Result<String> {
    if old.chars().count() != new.chars().count() {
        return Err(anyhow::anyhow!("Replace set character length mismatch"))
    }
    let result: String = text.chars().map(|ch| {
        if old.contains(ch) {
            new.chars().nth(old.chars().position(|c| c == ch).unwrap()).unwrap()
        } else {
            ch
        }
    }).collect();
    Ok(result)
}

pub fn count_substrings_of_length(text: &str, length: usize) -> Result<BTreeMap<String, usize>> {
    let mut result: BTreeMap<String, usize> = BTreeMap::new();

    for i in 0..text.chars().count()-length+1 {
        // beginning of the *first character* of the currently counted substring
        let start = text.char_indices().map(|(i, _)| i).nth(i).unwrap();
        // beginning of the *last character* of the currently counted substring 
        let end = text.char_indices().map(|(i, _)| i).nth(i+length-1).unwrap();
        // get the substring,  start at the first character and end at the last (calculated as its offset+ its length)
        let substr = &text[start..end+text.chars().nth(i+length-1).unwrap().len_utf8()];
        let count = result.entry(substr.to_string()).or_insert(0);
        *count += 1;
    }

    Ok(result)
}

pub fn replace_from_config(text: &mut String, cfg: &Config) -> Result<()> {
    // Replace chars according to replace set
    *text = character_substitution(&text, &cfg.replace.old, &cfg.replace.new)?;
    Ok(())
}

pub fn normalize_frequency_map(freq_map: BTreeMap<String, usize>) -> FreqMap {
    let substrs_count= freq_map.values().sum::<usize>() as f64;
    let mut result: FreqMap = BTreeMap::new();
    for (key, value) in freq_map {
        result.insert(key, ((value as f64)/substrs_count)*100.);
    }
    result
}

pub fn sort_chars_by_frequency(ciphertext: &str) -> Result<String> {
    let freq_map = count_substrings_of_length(ciphertext, 1)?;

    let mut v = Vec::from_iter(freq_map);
    v.sort_by(|&(_, a), &(_, b)| b.cmp(&a));

    Ok(v.into_iter().map(|(c, _)| c).collect())
}

pub fn bigram_freq_map_to_distribution_matrix(freq_map: &FreqMap, translation_str: &str) -> DistributionMatrix {
    let matrix_size = translation_str.chars().count();
    let mut d_matrix = DistributionMatrix::zeros((matrix_size, matrix_size));
    for i in 0..matrix_size {
        for j in 0..matrix_size {
            let frequency = freq_map.get(&format!("{}{}", translation_str.chars().nth(i).unwrap(), translation_str.chars().nth(j).unwrap()));
            match frequency {
                // If a value exists for that bigram, assign it to the correct place in matrix
                Some(x) => {
                    d_matrix[[i,j]] = *x
                },
                // Else do nothing, 0 is already there by default
                None => (),
            }
        }
    }
    d_matrix
}

fn swap_in_matrix(axis: usize, index1: usize, index2: usize, matrix: &mut DistributionMatrix) {
    let mut it = matrix.axis_iter_mut(ndarray::Axis(axis));

    ndarray::Zip::from(it.nth(index1).unwrap())
         .and(it.nth(index2 - (index1 + 1)).unwrap())
         .for_each(std::mem::swap);
}

pub fn swap_rows(index1: usize, index2: usize, matrix: &mut DistributionMatrix) {
    swap_in_matrix(0, index1, index2, matrix)
}

pub fn swap_columns(index1: usize, index2: usize, matrix: &mut DistributionMatrix) {
    swap_in_matrix(1, index1, index2, matrix)
}

pub fn swap_chars_in_string(index1: usize, index2: usize, input: &str) -> Result<String> {
    let mut i1 = index1;
    let mut i2 = index2;
    if i2 < i1 {
        let tmp = i1;
        i1 = i2;
        i2 = tmp;
    }
    if !(i1 < input.len()) || !(i2 < input.len()) {
        return Err(anyhow::anyhow!("Indices out of bounds"))
    }
    let mut swapped = String::with_capacity(input.len());
    swapped.push_str(&input.chars().take(i1).collect::<String>());
    swapped.push(input.chars().nth(i2).unwrap());
    swapped.push_str(&input.chars().skip(i1+1).take((i2-1)-i1).collect::<String>());
    swapped.push(input.chars().nth(i1).unwrap());
    swapped.push_str(&input.chars().skip(i2+1).collect::<String>());

    Ok(swapped)
}

pub fn extend_key(key: &str, alphabet: &str) -> String {
    let mut result = String::with_capacity(alphabet.len());
    result.push_str(key);
    result.push_str(&alphabet.chars().filter(|c| !key.contains(*c)).collect::<String>());

    result
}

#[cfg(test)]
mod tests {
    use ndarray::array;

    // importing names from outer (for mod tests) scope.
    use super::*;
    use crate::library::language::{Replace, Characters};

    #[test]
    fn test_count_characters() {
        let result = count_substrings_of_length("AhojjaksemasKamo", 1).unwrap();
        let expected: BTreeMap<String, usize> = BTreeMap::from([
            ("A".to_string(), 1 as usize),
            ("h".to_string(), 1 as usize),
            ("o".to_string(), 2 as usize),
            ("j".to_string(), 2 as usize),
            ("a".to_string(), 3 as usize),
            ("k".to_string(), 1 as usize),
            ("s".to_string(), 2 as usize),
            ("e".to_string(), 1 as usize),
            ("m".to_string(), 2 as usize),
            ("K".to_string(), 1 as usize),
        ]);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_count_characters_utf8() {
        let result = count_substrings_of_length("AhojjaksemášKámo", 1).unwrap();
        let expected: BTreeMap<String, usize> = BTreeMap::from([
            ("A".to_string(), 1 as usize),
            ("h".to_string(), 1 as usize),
            ("o".to_string(), 2 as usize),
            ("j".to_string(), 2 as usize),
            ("a".to_string(), 1 as usize),
            ("á".to_string(), 2 as usize),
            ("k".to_string(), 1 as usize),
            ("s".to_string(), 1 as usize),
            ("š".to_string(), 1 as usize),
            ("e".to_string(), 1 as usize),
            ("m".to_string(), 2 as usize),
            ("K".to_string(), 1 as usize),
        ]);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_count_bigrams() {
        let result = count_substrings_of_length("AhojjaksemasKamoakemo", 2).unwrap();
        let expected: BTreeMap<String, usize> = BTreeMap::from([
            ("Ah".to_string(), 1 as usize),
            ("ho".to_string(), 1 as usize),
            ("oj".to_string(), 1 as usize),
            ("jj".to_string(), 1 as usize),
            ("ja".to_string(), 1 as usize),
            ("ak".to_string(), 2 as usize),
            ("ks".to_string(), 1 as usize),
            ("se".to_string(), 1 as usize),
            ("em".to_string(), 2 as usize),
            ("ma".to_string(), 1 as usize),
            ("as".to_string(), 1 as usize),
            ("sK".to_string(), 1 as usize),
            ("Ka".to_string(), 1 as usize),
            ("am".to_string(), 1 as usize),
            ("mo".to_string(), 2 as usize),
            ("oa".to_string(), 1 as usize),
            ("ke".to_string(), 1 as usize),
        ]);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_count_bigrams_utf8() {
        let result = count_substrings_of_length("AhojjaksemášKámoakémoší", 2).unwrap();
        let expected: BTreeMap<String, usize> = BTreeMap::from([
            ("Ah".to_string(), 1 as usize),
            ("ho".to_string(), 1 as usize),
            ("oj".to_string(), 1 as usize),
            ("jj".to_string(), 1 as usize),
            ("ja".to_string(), 1 as usize),
            ("ak".to_string(), 2 as usize),
            ("ks".to_string(), 1 as usize),
            ("se".to_string(), 1 as usize),
            ("em".to_string(), 1 as usize),
            ("ém".to_string(), 1 as usize),
            ("má".to_string(), 1 as usize),
            ("áš".to_string(), 1 as usize),
            ("šK".to_string(), 1 as usize),
            ("Ká".to_string(), 1 as usize),
            ("ám".to_string(), 1 as usize),
            ("mo".to_string(), 2 as usize),
            ("oa".to_string(), 1 as usize),
            ("ké".to_string(), 1 as usize),
            ("oš".to_string(), 1 as usize),
            ("ší".to_string(), 1 as usize),
        ]);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_linear_replace_basic() {
        let result = character_substitution("my european dream does not include the steak which looks like a bubble bam and tastes like a bubble bam", "eu bm", "ueTg_").unwrap();
        let expected = "_yTueropuanTdrua_TdousTnotTincleduTthuTstuakTwhichTlooksTlikuTaTgeggluTga_TandTtastusTlikuTaTgeggluTga_";
        assert_eq!(result, expected)
    }

    #[test]
    fn test_linear_replace_utf8() {
        let result = character_substitution("šťavnaté ústřice ty já mám nejradši, ty já mám rád🤠", "ťéúřáš🤠", "teurasK").unwrap();
        let expected = "stavnate ustrice ty ja mam nejradsi, ty ja mam radK";
        assert_eq!(result, expected)
    }

    #[test]
    fn test_normalize_input_basic() {
        let mut kek = String::from("AbsolutneCerneTeleso");
        let cfg = Config {
            title: String::from("Test"),
            ioc: 0.1,
            replace: Replace {
                old: String::from("ěščřžýáíéACT"),
                new: String::from("escrzyaieact")
            },
            characters: Characters {
                chars: String::from("qwertzuiopasdfghjkllyxcvbnm")
            },
            bigrams: BTreeMap::from([
                ("ok".to_string(), 0.1)
            ])
        };
        let normalized = replace_from_config(&mut kek, &cfg);
        assert!(matches!(normalized.err(), None));
        let expected = "absolutnecerneteleso";
        assert_eq!(kek, expected)
    }

    #[test]
    fn test_normalize_input_utf8() {
        let mut kek = String::from("Mňam do Brna, číčo! Cože děláš? ťuk 7...");
        let cfg = Config {
            title: String::from("Test"),
            ioc: 0.1,
            replace: Replace {
                old: String::from("ěščřžýáíéňťúů"),
                new: String::from("escrzyaientuu")
            },
            characters: Characters {
                chars: String::from("qwertzuiopasdfghjkllyxcvbnm")
            },
            bigrams: BTreeMap::from([
                ("ok".to_string(), 0.1)
            ])
        };
        let normalized = replace_from_config(&mut kek, &cfg);
        assert!(matches!(normalized.err(), None));
        let expected = "Mnam do Brna, cico! Coze delas? tuk 7...";
        assert_eq!(kek, expected)
    }

    #[test]
    fn test_normalize_frequency_map_basic() {
        let input: BTreeMap<String, usize> = BTreeMap::from([
            ("aa".to_string(), 30 as usize),
            ("bb".to_string(), 10 as usize),
            ("cc".to_string(), 9 as usize),
            ("dd".to_string(), 20 as usize),
            ("ee".to_string(), 30 as usize),
            ("ff".to_string(), 1 as usize),
        ]);
        let result = normalize_frequency_map(input);
        let expected: FreqMap = BTreeMap::from([
            ("aa".to_string(), 30.0),
            ("bb".to_string(), 10.0),
            ("cc".to_string(), 9.0),
            ("dd".to_string(), 20.0),
            ("ee".to_string(), 30.0),
            ("ff".to_string(), 1.0),
        ]);
        assert_eq!(result, expected)
    }

    #[test]
    fn test_sort_chars_by_frequency_basic() {
        let input = "abbcccccddeee";
        let expected = "cebda";
        let result = sort_chars_by_frequency(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn test_sort_chars_by_frequency_utf8() {
        let input = "ábbčččččddéěě";
        let expected = "čbděáé";
        let result = sort_chars_by_frequency(input).unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn test_bigram_freq_map_to_distribution_matrix_basic() {
        let input: BTreeMap<String, f64> = BTreeMap::from([
            ("aa".to_string(), 80.0),
            ("ab".to_string(), 10.0),
            ("bb".to_string(), 5.0),
            ("cc".to_string(), 1.5),
            ("dd".to_string(), 1.5),
            ("ee".to_string(), 1.0),
            ("ff".to_string(), 1.0),
        ]);
        let expected = array![[80.0, 10.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 1.5, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]];
        let result = bigram_freq_map_to_distribution_matrix(&input, "abcdef");
        assert_eq!(result, expected)
    }

    #[test]
    fn test_bigram_freq_map_to_distribution_matrix_utf8() {
        let input: BTreeMap<String, f64> = BTreeMap::from([
            ("aa".to_string(), 70.0),
            ("ab".to_string(), 10.0),
            ("ač".to_string(), 10.0),
            ("bb".to_string(), 5.0),
            ("čč".to_string(), 1.5),
            ("dd".to_string(), 1.5),
            ("ee".to_string(), 1.0),
            ("ff".to_string(), 1.0),
        ]);
        let expected = array![[70.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]];
        let result = bigram_freq_map_to_distribution_matrix(&input, "abcčdef");
        assert_eq!(result, expected)
    }

    #[test]
    fn test_swap_in_matrix() {
        let mut input1 = array![[70.0, 10.0, 0.0, 10.0, 0.0, 0.0, 0.0],
                                                    [0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                    [70.0, 0.0, 0.0, 1.5, 0.0, 0.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]];
        let expected1 = array![[10.0, 10.0, 0.0, 70.0, 0.0, 0.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [1.5, 0.0, 0.0, 70.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0]];
        // Try swapping columns
        swap_in_matrix(1, 0, 3, &mut input1);
        assert_eq!(input1, expected1);
        let expected2 = array![[10.0, 10.0, 0.0, 70.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [1.5, 0.0, 0.0, 70.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, 0.0, 0.0, 0.0]];
        // Try swapping rows
        swap_in_matrix(0, 1, 6, &mut input1);
        assert_eq!(input1, expected2);
    }

    #[test]
    fn test_swap_in_string_basic() {
        let input = "abcdefd";
        let expected = "abfdecd";
        let result = swap_chars_in_string(2, 5, input).unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn test_swap_in_string_utf8() {
        let input = "ěščřžýáíé";
        let expected = "ěíčřžýášé";
        let result = swap_chars_in_string(1, 7, input).unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn test_extend_key_basic() {
        let input = "asdfqwer";
        let expected = "asdfqwermuilh";
        let result = extend_key(input, "mfdsrwqeuilha");
        assert_eq!(expected, result);
    }

    #[test]
    fn test_extend_key_utf8() {
        let input = "ašdfqwér";
        let expected = "ašdfqwérmsuilřh";
        let result = extend_key(input, "mfdšsrwqéuilřha");
        assert_eq!(expected, result);
    }
}
