use crate::library::helpers::*;

use anyhow::{Result};

pub fn calculate_ioc(input: &str) -> Result<f64> {
    let chars_count = count_substrings_of_length(input, 1)?; // n

    log::trace!("Character frequency: {:?}", chars_count);

    let text_length: f64 = input.len() as f64; // N

    log::trace!("text length: {:?}", text_length);

    let mut prob_sum = 0.;

    for i in chars_count {
        prob_sum += (i.1 * (i.1-1)) as f64
    }

    log::trace!("Probability sum: {}", prob_sum);

    let normalizer = text_length * (text_length-1.);

    log::trace!("Normalizer: {}", normalizer);

    let result = prob_sum/normalizer;

    log::debug!("Calculated IoC: {}", result);

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ioc_1() {
        let result = calculate_ioc("testovacitextprotestovacilidicotojakomabyttotedyopravdunevimalejetoneconadhernehonecoopravdunadhernehozejetotakalfrede").unwrap();
        assert_eq!(result, 0.0696798493408663)
    }

    #[test]
    fn test_ioc_2() {
        let result = calculate_ioc("accordingtotheoxfordenglishdictionaryhelloisanalterationofhallohollowhichcamefromoldhighgermanhalaholaemphaticimperativeofhalonholontofetchusedespeciallyinhailingaferrymanitalsoconnectsthedevelopmentofhellototheinfluenceofanearlierformhollawhoseoriginisinthefrenchholaroughlywhoatherefromfrenchlathereasinadditiontohellohalloohallohollohulloandrarelyhilloalsoexistasvariantsorrelatedwordsthewordcanbespeltusinganyofallfivevowelstelephonetheuseofhelloasatelephonegreetinghasbeencreditedtothomasedisonaccordingtoonesourceheexpressedhissurprisewithamisheardhulloalexandergrahambellinitiallyusedahoyasusedonshipsasatelephonegreetinghoweverinedisonwrotetotbadavidpresidentofthecentraldistrictandprintingtelegraphcompanyofpittsburghfrienddavididonotthinkweshallneedacallbellashellocanbeheardtofeetawaywhatyouthinkedisonpsfirstcostofsenderreceivertomanufactureisonlybycentraltelephoneexchangeoperatorswereknownashellogirlsbecauseoftheassociationbetweenthegreetingandthetelephone"
         ).unwrap();
        assert_eq!(result, 0.06869950205442364)
    }

    #[test]
    fn test_ioc_3() {
        let result = calculate_ioc("aaa").unwrap();
        assert_eq!(result, 1.)
    }

    #[test]
    fn test_ioc_4() {
        let result = calculate_ioc("abc").unwrap();
        assert_eq!(result, 0.)
    }
}
