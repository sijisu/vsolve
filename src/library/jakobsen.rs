use crate::library::helpers::*;
use ndarray;
use anyhow::{Result, Ok};
use crate::library::language::Config;

type DistributionMatrix = ndarray::Array2<f64>;

fn get_initial_plaintext(initial_key: &str, ciphertext: &str, cfg: &Config) -> Result<String> {    
    // Check if the number of characters in text isn't larger then the number of characters of the language
    let initial_key_len = initial_key.chars().count();
    let cfg_chars_len = cfg.characters.chars.chars().count();

    let mut char_frequency_order= cfg.characters.chars.clone();

    if initial_key_len < cfg_chars_len {
        char_frequency_order = char_frequency_order.chars().into_iter().take(initial_key_len).collect();
    } else if initial_key_len > cfg_chars_len {
        return Err(anyhow::anyhow!("There are more unique characters in the ciphertext, then there are unique characters in the language. You might need to normalize your ciphertext more."))
    }
    let initial_plaintext = character_substitution(ciphertext, &initial_key, &char_frequency_order)?;
    Ok(initial_plaintext)
}

fn calculate_inaccuracy(real_matrix: &DistributionMatrix, expected_matrix: &DistributionMatrix) -> f64 {
    let mut result: f64 = 0.;

    for i in 0..expected_matrix.shape()[0] {
        for j in 0..expected_matrix.shape()[1] {
            result += (real_matrix[[i,j]]-expected_matrix[[i,j]]).abs();
        }
    }
    
    result
}

fn optimize_key(key: &str, inaccuracy: f64, real_matrix: &DistributionMatrix, expected_matrix: &DistributionMatrix) -> Result<String> {
    for i in 0..key.chars().count() {
        for j in i+1..key.chars().count() {
            let key_candidate = swap_chars_in_string(i, j, key)?;
            let mut matrix_candidate = real_matrix.clone();
            swap_rows(i, j, &mut matrix_candidate);
            swap_columns(i, j, &mut matrix_candidate);
            let new_inaccuracy = calculate_inaccuracy(&matrix_candidate, expected_matrix);
            if new_inaccuracy < inaccuracy {
                log::trace!("Optimized key {}, new inaccuracy {}", key_candidate, new_inaccuracy);
                return optimize_key(&key_candidate, new_inaccuracy, &matrix_candidate, expected_matrix);
            }
        }
    }
    log::debug!("Key optimization finished, final key: {}, inaccuracy: {}", key, inaccuracy);
    Ok(String::from(key))
}

pub fn solve(ciphertext: &str, cfg: &Config) -> Result<String> {
    /*
        Prep work
    */
    let expected_matrix = bigram_freq_map_to_distribution_matrix(&cfg.bigrams, &cfg.characters.chars);

    /* 
        Step 1
    */
    let initial_key = sort_chars_by_frequency(ciphertext)?;
    log::debug!("Initial key: {}", initial_key);
    let initial_plaintext = get_initial_plaintext(&initial_key, ciphertext, cfg)?;
    log::debug!("Initial plaintext: {}", initial_plaintext);

    /*
        Step 2
    */
    let absolute_bigrams = count_substrings_of_length(&initial_plaintext, 2)?;
    let relative_bigrams = normalize_frequency_map(absolute_bigrams);
    let distribution_matrix = bigram_freq_map_to_distribution_matrix(&relative_bigrams, &cfg.characters.chars);

    /*
        Step 3
    */

    let initial_inaccuracy = calculate_inaccuracy(&distribution_matrix, &expected_matrix);
    log::debug!("Initial inaccuracy: {}", initial_inaccuracy);
    let result = optimize_key(&extend_key(&initial_key, &cfg.characters.chars), initial_inaccuracy, &distribution_matrix, &expected_matrix)?;

    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::library::language::Config;
    use std::str::FromStr;
    use ndarray::array;

    #[test]
    fn test_get_initial_plaintext_basic() {
        let cfg = Config::from_str(r#"
        title = "Test language"
        ioc = 0.1
        
        [replace]
        old = "ěščřž"
        new = "escrz"
        
        [characters]
        chars = "eaointsrvluckdzpmyhjbfgxwq"
        
        [bigrams]
        ee = 0.00488748315563841
        ai = 0.0719158235758223
        eo = 99.92319669326854
        "#).unwrap();

        let result = get_initial_plaintext("rbnvgaiefpqmhuyclzxowtsjk",
         "ancerybzhfgbyrgvqbfnuycbqyrfgngvfgvxirqrpxrfvgrapqevfxsnpgbepbyynobengvbacbprgcbqilmviralpuqrgvnqbfcvinwvpvpufirubuvfgbevpxrubznkvznnbqgrqboliryzvcbmibyanxyrfnzhmrmngbmilfbinavmvibgavhebiarvcehzlfybinileboncbgenivaglfnzrbxbyabfgvnyrmnebirawrfgrzaburzilffvzrebhcevfcvinwvxcehqxrzhanehfghanqinulnbormvgllgrqlilenmarwfvanqinullqrgvnqbfcvinwvpvpucbqyrbqobearubpnfbcvfhgurynaprganivpiebprrrrrcbceircbprgqrgvfanqinubhcerqpvcbprgqrgvfcbqilmvibhhhiebprrrrrzrybcbqyrqngfirgbirmqenibgavpxrbetnavmnpranqinuhhhzvyvbahqrgvqbcrgvyrgxnmqrfrqzanpgrcevoyvmarcbybivanmavpumvyninfvvmuehonpgiegvaninsevprrrmngvzpbirilfcrylpumrzvpufrfanqinubhnbormvgbhpnfgrwvcbglxnwvqrgvmebqvafavmfvzfbpvbrxbabzvpxlzfgnghfrziebmibwbilpufgngrpuaravmvibgavhebiragnxhephwvpvceboyrzfrglxnwnxobungfvpuiefgrimirgfvpuzrfggnxpuhqfvubiraxbinnnqrgvnqbfcvinwvpviybpxqbjahcevoenylohqbhmavpubormavqbfcryvfceboyrzlinehwrbormvgbybtttpvfgpynarxxxxcbzrearabinfbpvnyavfxhcvanobunglpufgnyriavznanqinuhwnxbflzobyoynubolghwrwvzcevfyhfavxhzarqbpunmvmrbormvgncerqfgnihwrmninmalmqenibgavceboyrzmrwzraniqrgfxrzirxhhevxncebfreirevebmuynfpmmqrarxunzevxirqbhpvsnxhyglgryrfarxhyghelhaviremvglcnynpxrubgrafrceboyrzngvxbhmqenirubmvibgavubfglyhmnolinwnxananebqavnriebcfxrhebiavgnxirilmxhzarpvaabfgvcebfirgbibhmqenibgavpxbhbetnavmnpvvvunzevxcevpvgnobbzanqinulnbormvglinfvwfxlpunnsevpxlpumrzvpugnxrebmznpuhfrqnirubmvibgavubfglyhhhqrgvebfgrhmvinavpulgelpugryrsbahilenmarholincevebmrarubcbulohngbixbzovanpvfrfcngalzfgenibinavzarznwvanebqlebmibwbirubfirgnfnapvebmhzarmiynqabhggqbqninncenmqarxnybevrrrrpuhqvyvqrwvmzabuqlargecvancebfglzarqbfgngxrzwvqynabilzceboyrzrzwrfgeninobungnanghxlnxnybevrbifrzilmvibirarxinyvgavvpulovwvzcenivqryarfgenibinavfrmnfgbhcravzbibprnmryravallevxnunzevxqhfyrqxrzwrqnyfvcebuyhobinavarebiabfgvvcbqilmvinnbormvgnfrilfxlghwvirfgrwalpuxbzhavgnpuzabuqlqbxbaprirfgrwarqbznpabfgvvvcbqyrcebtabmbetnavmnprjbeyqborfvglsrqrengvbaohqriebprrrrranfirgrmvggggzvyvbahqrgvfbormvgbhcbybivanmgbubicbhulpuqrivgvmrzvpucerinmargrpuarwyvqangrwfvpuuuieryngviavpucbpgrpunyrbormvgnarwivprgencvznyrgvpubzbefxrfgngllanbfgebiranhehbetnavmnprercbegbinynivprarmmmcebpragbormavpuqvirxnpuyncphim", 
         &cfg).unwrap();
        
        assert_eq!(result, "toprezaykvnazeniuavodzpauzevnonivnihseuelhevinetlurivhgolnarlazzojaroniatpalenpausmcisetmldueniouavpisobilildvsedadivnarilhedayowiyooauneuajmsezyipacsaztohzevoykceconacsmvasoticisantikrasteiprkymvzasosmrajopanrositnmvoyeahaztavniozecorasetbevneytadeysmvviyerakprivpisobihprkuheyktorkvnktousodmoajecinmmneumsmroctebvitousodmmueniouavpisobilildpauzeaujartedalovapivkndezotlentosilsraleeeeepaprsepalenuenivtousodakpreulipalenuenivpausmcisakkksraleeeeeyezapauzeuonvsenasecurosantilhearfoticoletousodkkkyiziatkueniuapenizenhocueveuytolneprijzictepazasitoctildcizosoviicdrkjolnsrnitosogrileeeconiylasesmvpezmldceyildvevtousodakoajecinaklovnebipanmhobiuenicrauitvticviyvaliaehatayilhmyvnonkveysracsabasmldvnoneldteticisantikrasetnohkrlkbiliprajzeyvenmhobohjadonvildsrvnescsenvildyevnnohldkuvidasethasoooueniouavpisobiliszalhuaxtkprijrozmjkuakctildajectiuavpezivprajzeymsorkbeajecinazaffflivnlzotehhhhpayertetasovalioztivhkpitojadonmldvnozestiyotousodkbohavmyjazjzodajmnkbebiyprivzkvtihkyteualdociceajecinopreuvnoskbecosoctmcurosantiprajzeycebyetosuenvheysehkkrihopraverseriracdzovlccuetehdoyrihseuakligohkznmnezevtehkznkrmktisercinmpozolhedanetveprajzeyonihakcurosedacisantidavnmzkcojmsobohtotorautioesrapvhekrastinohsesmchkytelittavnipravsenasakcurosantilhakarfoticoliiidoyrihprilinojaaytousodmoajecinmsovibvhmldoogrilhmldceyildnoheracyoldkveuosedacisantidavnmzkkkueniravnekcisotildmnrmldnezegatksmroctekjmsopriracetedapadmjkonashayjitolivevpontmyvnrosasotiyteyobitoraumracsabasedavsenovotlirackytecszoutaknnuauosooprocutehozarieeeeldkuiziuebicytadumtenrpitopravnmyteuavnonheybiuzotasmyprajzeyeybevnrosojadonotonkhmohozarieasveysmcisasetehsozintiildmjibiyprosiueztevnrosasotivecovnakpetiyasaleocezetitmmrihodoyrihukvzeuheybeuozvipradzkjasotiterastavniipausmcisooajecinovesmvhmnkbisevnebtmldhayktinoldytadumuahatlesevnebteuayoltavniiipauzepraftacarfoticolexarzuajevinmgeueroniatjkuesraleeeeetovsenecinnnnyiziatkuenivajecinakpazasitocnadaspakdmlduesiniceyildpresocteneldtebziutonebvildddsrezonistildpalneldozeajecinotebsilenropiyozenildayarvhevnonmmtoavnrasetokrkarfoticolereparnasozosiletecccpraletnajectilduiseholdzoplksc"
         );
    }

    #[test]
    fn test_calculate_inaccuracy() {
        let real = array![[80.0, 10.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, -0.02, 0.0],
                                                        [0.0, 0.0, 1.5, 0.0, 0.0, -2.5],
                                                        [0.0, 0.0, 0.0, 1.5, 0.0, 0.0],
                                                        [3.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 1.0]];

        let expected = array![[80.0, 10.0, 0.0, -20.0, 0.0, 0.0],
                                                        [0.0, 5.0, 0.0, 0.0, -0.01, 0.0],
                                                        [0.0, 0.0, 1.5, 0.0, 0.0, 0.0],
                                                        [0.0, 0.01, 0.0, 0.5, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 1.0, 0.0],
                                                        [0.0, 0.0, 1.0, -0.1, 0.0, 1.0]];

        let result = calculate_inaccuracy(&real, &expected);

        // rounding hack to fix floating point errors
        assert_eq!((result * 100.0).round() / 100.0, 27.62)
    }

    #[test]
    fn test_optimize_key() {
        let real = array![[0.0, 0.0, 0.0, 0.0, 0.0, 50.0],
                                                    [0.0, 0.0, 0.0, 10.0, 30.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                    [0.0, 0.0, 10.0, 0.0, 0.0, 0.0],
                                                    [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]];

        let expected = array![[0.0, 10.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                                                        [0.0, 0.0, 0.0, 30.0, 0.0, 0.0],
                                                        [0.0, 0.0, 10.0, 0.0, 0.0, 0.0],
                                                        [50.0, 0.0, 0.0, 0.0, 0.0, 0.0]];

        let key = "abcdef";

        let result = optimize_key(key, 100., &real, &expected).unwrap();

        assert_eq!(result, "fbcdea")
    }
}
