use std::fs::File;
use std::io::Read;
use std::str::FromStr;
use anyhow::{Error, Result, Context, Ok};
use serde::Deserialize;
use std::path::Path;
use std::collections::BTreeMap;

const ALLOWED_BIGRAM_PERCENTAGES_ERROR: i8 = 2;

/*
Why this language config structure?

To make in compatible (ik, you still have to do ini -> toml conversion, but I don't feel like supporting ini) 
with the Cipher-solver program by Tomáš Hanzák, 2004, available here: http://thanzak.sweb.cz/Download.htm
 */

#[derive(Debug, Deserialize)]
pub struct Replace {
    pub old: String,
    pub new: String
}

#[derive(Debug, Deserialize)]
pub struct Characters {
    pub chars: String
}

#[derive(Debug, Deserialize)]
pub struct Config {
    pub title: String,
    pub ioc: f64,
    pub replace: Replace,
    pub characters: Characters,
    pub bigrams: BTreeMap<String, f64>
}

impl FromStr for Config {
    type Err = Error;

    /// Load a `Config` from some string.
    fn from_str(src: &str) -> Result<Self> {
        toml::from_str(src).with_context(|| "Invalid configuration file")
    }
}

impl Config {
    pub fn from_disk<P: AsRef<Path>>(config_file: P) -> Result<Config> {
        let mut buffer = String::new();
        File::open(config_file)
            .with_context(|| "Unable to open the configuration file")?
            .read_to_string(&mut buffer)
            .with_context(|| "Couldn't read the configuration file")?;

        Config::from_str(&buffer)
    }

    pub fn is_valid(&self) -> Result<()>{
        // Check if the number of old and new chars matches
        if self.replace.old.chars().count() != self.replace.new.chars().count() {
            return Err(anyhow::anyhow!("Replace set character length mismatch"))
        }
        // Check if the bigrams percentages add up to 100 with a tolerance of 10^-ALLOWED_BIGRAM_PERCENTAGES_ERROR
        if (100. - self.bigrams.values().sum::<f64>()) > (10 as f64).powi((-ALLOWED_BIGRAM_PERCENTAGES_ERROR).into()) as f64 {
            return Err(anyhow::anyhow!("Bigram percentages don't add up to 100, but to {}", self.bigrams.values().sum::<f64>()))
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    // importing names from outer (for mod tests) scope.
    use super::*;

    #[test]
    fn test_load_valid_config() {
        let cfg = Config::from_str(r#"
        title = "Test language"
        ioc = 0.1
        
        [replace]
        old = "ěščřž"
        new = "escrz"
        
        [characters]
        chars = "abcdefgh"
        
        [bigrams]
        ee = 0.00488748315563841
        ai = 0.0719158235758223
        eo = 99.92319669326854
        "#).unwrap();
        assert_eq!(cfg.title, "Test language");
        assert_eq!(cfg.replace.old, "ěščřž");
        assert_eq!(cfg.replace.new, "escrz");
        assert_eq!(cfg.characters.chars, "abcdefgh");
        assert_eq!(cfg.bigrams["ai"], 0.0719158235758223);
        assert_eq!(cfg.bigrams["ee"], 0.00488748315563841);
        assert!(matches!(cfg.is_valid().err(), None));
    }

    #[test]
    fn test_load_invalid_replace_length_config() {
        let cfg = Config::from_str(r#"
        title = "Test language"
        ioc = 0.1
        
        [replace]
        old = "ěščřž"
        new = "escrza"
        
        [characters]
        chars = "abcdefgh"
        
        [bigrams]
        ee = 0.00488748315563841
        ai = 0.0719158235758223
        eo = 99.92319669326854
        "#).unwrap();
        let invalid = cfg.is_valid();
        assert!(!matches!(invalid.err(), None));
    }

}
