use std::process::Command;

static TEST1_PLAINTEXT: &'static str = "IoC: 0.058608712377910634
";

static TEST2_PLAINTEXT: &'static str = "IoC: 0.058922050155199246
";

#[test]
fn test_ioc_input1() {
    let output = Command::new("./target/debug/vsolve")
            .args(["ioc", "-ci", "./tests/ciphertexts/input1.txt"])
            .output()
            .expect("failed to execute process");

    assert_eq!(String::from_utf8_lossy(&output.stdout), TEST1_PLAINTEXT);
}

#[test]
fn test_ioc_input2() {
    let output = Command::new("./target/debug/vsolve")
            .args(["ioc", "-ci", "./tests/ciphertexts/input2.txt"])
            .output()
            .expect("failed to execute process");

    assert_eq!(String::from_utf8_lossy(&output.stdout), TEST2_PLAINTEXT);
}
